import CustomerReview, { ICustomerReview, ICustomerReviewConstructor } from "@models/CustomerReview";
import { IFactory } from "./Factory";

export interface ICustomerReviewFactory extends IFactory<ICustomerReviewConstructor, ICustomerReview> {}

export default class CustomerReviewFactory implements ICustomerReviewFactory {
    create (constructor: ICustomerReviewConstructor) {
        return new CustomerReview(constructor);
    }
}