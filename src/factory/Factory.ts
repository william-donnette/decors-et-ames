export interface IFactory<C, T> {
    create: (constructor: C) => T
}