import Service, { IService, IServiceConstructor } from "@models/Service";
import { IFactory } from "./Factory";

export interface IServiceFactory extends IFactory<IServiceConstructor, IService> {}

export default class ServiceFactory implements IServiceFactory {
    create (constructor: IServiceConstructor) {
        return new Service(constructor);
    }
}