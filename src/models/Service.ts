import { ComponentType, ReactNode } from "react";

export interface IService {
    name: string,
    description: string,
    icon?: ComponentType,
    price: number
}

export interface IServiceConstructor extends IService {}

export default class Service implements IService {
    #name: string;
    #description: string;
    #icon?: ComponentType;
    #price: number;

    constructor ({name, description, icon, price}: IServiceConstructor) {
        this.#name = name;
        this.#description = description;
        this.#icon = icon;
        this.#price = price;
    }

    get name () {
        return this.#name;
    }

    get description () {
        return this.#description;
    }

    get icon () {
        return this.#icon;
    }

    get price () {
        return this.#price;
    }
}