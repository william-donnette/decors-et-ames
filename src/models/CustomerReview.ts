export interface ICustomerReview {
    review: string,
    customerName: string,
    customerImage: string,
}

export interface ICustomerReviewConstructor extends ICustomerReview {}

export default class CustomerReview implements ICustomerReview {
    #review: string;
    #customerName: string;
    #customerImage: string;

    constructor ({review, customerName, customerImage}: ICustomerReviewConstructor) {
        this.#customerImage = customerImage;
        this.#customerName = customerName;
        this.#review = review;
    }

    get review () {
        return this.#review;
    }

    get customerImage () {
        return this.#customerImage;
    }

    get customerName () {
        return this.#customerName;
    }
    
}