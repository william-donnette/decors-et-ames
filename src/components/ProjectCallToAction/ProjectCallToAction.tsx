import Layout from "@components/Layout/Layout";
import Screen from "@components/Screen/Screen";
import { asset } from "@utils/functions";
import { FunctionComponent } from "react";

interface ProjectCallToActionProps {
    
}
 
const ProjectCallToAction: FunctionComponent<ProjectCallToActionProps> = () => {
    return (
        <Screen className="bg-terra-50 h-screen">
            <Layout className="flex flex-col justify-between items-center h-full">
                <h2 className="text-5xl z-10">Quelques projets clients</h2>
                <Layout className="relative grid place-items-center h-full">
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="rounded-xl w-full h-full opacity-30 blur-sm absolute bottom-0 right-0 z-0 object-cover"
                        style={{clipPath: "polygon(-10% -10%, 50% -10%, 75% 110%, -10% 110%)"}}
                    />
                   
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="absolute top-0 right-0 rounded-xl w-80 z-10 hover:scale-110 hover:-translate-x-40 transition-all duration-700 hover:z-20"
                    />
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="opacity-60 lg:opacity-100 rounded-xl absolute right-0 bottom-0 w-96 z-10 animate-pulse hover:z-20 hover:scale-125 hover:-translate-x-40 hover:animate-none transition-all duration-700"
                    />
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="hidden lg:block rounded-full object-cover absolute shadow-xl top-40 right-40 aspect-square w-40 z-10 hover:rounded-xl hover:scale-150 hover:animate-none transition-all duration-700 hover:z-20"
                    />
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="opacity-60 lg:opacity-100 rounded-full object-cover absolute shadow-xl top-12 right-96 aspect-square w-20 z-10 animate-pulse hover:rounded-xl hover:scale-150 hover:animate-none transition-all duration-700 hover:z-20"
                    />
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="rounded-full object-cover absolute shadow-xl bottom-0 right-80 aspect-square w-32 z-10 hover:rounded-xl hover:scale-150 hover:animate-none transition-all duration-700 hover:z-20"
                    />
                    <img
                        src={asset("/assets/img/scandinav-living-room.jpg")}
                        alt="Montpellier Décoration Intérieur Salon Scandinave"
                        className="rounded-xl shadow-xl w-80 z-10 hover:scale-150 transition-all duration-700"
                    />
                </Layout>
            </Layout>
        </Screen>
    );
}
 
export default ProjectCallToAction;