import { classNames } from "@utils/functions";
import { FunctionComponent, ReactNode } from "react";
import Layout from "../Layout/Layout";

interface ContainerProps {
    children: ReactNode,
    className?: string
}
 
const Container: FunctionComponent<ContainerProps> = ({children, className}) => {
    return (
        <Layout className={classNames(className, "md:max-w-4xl")}>
            {children}
        </Layout>
    );
}
 
export default Container;