import Layout from "@components/Layout/Layout";
import { ICustomerReview } from "@models/CustomerReview";
import { FunctionComponent } from "react";

interface CustomerReviewItemProps {
    customerReview: ICustomerReview
}
 
const CustomerReviewItem: FunctionComponent<CustomerReviewItemProps> = ({customerReview}) => {
    return (
        <Layout className="w-full flex bg-terra-500 rounded-xl">
            <img 
            src={customerReview.customerImage} 
            alt={`Avis Décoration Intérieure ${customerReview.customerName}`}
            className="w-20 h-20 rounded-xl object-cover mr-5"
            />
            <blockquote>
                <p className="text-white">"{customerReview.review}"</p>
                <figcaption className="text-white italic">- {customerReview.customerName}</figcaption>
            </blockquote>
        </Layout>
    );
}
 
export default CustomerReviewItem;