import { classNames } from "@utils/functions";
import { FunctionComponent, ReactNode } from "react";
import Layout from "../Layout/Layout";

interface ScreenProps {
    children: ReactNode,
    className?: string
}
 
const Screen: FunctionComponent<ScreenProps> = ({children, className}) => {
    return (
        <Layout className={classNames("w-screen min-h-screen", className)}>
            {children}
        </Layout>
    );
}
 
export default Screen;