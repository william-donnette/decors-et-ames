import { FunctionComponent, ReactNode } from "react";

interface ButtonCallToActionProps {
    children: ReactNode
}
 
const ButtonCallToAction: FunctionComponent<ButtonCallToActionProps> = ({children}) => {
    return (
        <button className="text-white bg-terra-500 m-5 p-2 rounded-xl text-xl font-bold hover:scale-110 hover:text-white transition-all duration-200 outline-none border-0">{children}</button>
    );
}
 
export default ButtonCallToAction;