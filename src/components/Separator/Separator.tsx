import Layout from "@components/Layout/Layout";
import { FunctionComponent } from "react";

interface SeparatorProps {
    
}
 
const Separator: FunctionComponent<SeparatorProps> = () => {
    return (
        <Layout className="bg-terra-50">
            <section className="bg-terra-50 border-dotted border-8 border-terra-500 h-20">

            </section>
        </Layout>
    );
}
 
export default Separator;