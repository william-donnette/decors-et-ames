import CustomerReviewList from "@components/CustomerReviewList/CustomerReviewList";
import Layout from "@components/Layout/Layout";
import Screen from "@components/Screen/Screen";
import CustomerReviewRepository from "@repository/CustomerReviewRepository";
import { FunctionComponent } from "react";

interface CustomerReviewProps {
    
}
 
const CustomerReview: FunctionComponent<CustomerReviewProps> = () => {

    const customerReviews = CustomerReviewRepository.getCustomerReviews();

    return (
        <Screen className="bg-terra-50">
            <Layout className="flex flex-col justify-between items-center h-full">
                <h2 className="text-5xl z-10">Avis Clients</h2>
                <CustomerReviewList customerReviews={customerReviews} />
            </Layout>
        </Screen>
    );
}
 
export default CustomerReview;