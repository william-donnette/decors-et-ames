import { classNames } from "@utils/functions";
import { FunctionComponent, ReactNode } from "react";

interface LayoutProps {
    children: ReactNode,
    className?: string
}
 
const Layout: FunctionComponent<LayoutProps> = ({children, className}) => {
    return (
        <section className={classNames("w-full p-2 md:p-5", className)}>
            {children}
        </section>
    );
}
 
export default Layout;