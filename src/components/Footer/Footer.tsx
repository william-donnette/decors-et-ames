import Layout from "@components/Layout/Layout";
import { FunctionComponent } from "react";

interface FooterProps {
    
}
 
const Footer: FunctionComponent<FooterProps> = () => {
    return (
        <Layout className="bg-terra-50">
            <section className="bg-terra-50 border-dotted border-8 border-terra-500 h-20 grid place-items-center text-terra-500 font-bold">
                Tous droits réservés - Laura Larson
            </section>
        </Layout>
    );
}
 
export default Footer;