import Container from "@components/Container/Container";
import ServiceItem from "@components/ServiceItem/ServiceItem";
import { IService } from "@models/Service";
import { FunctionComponent } from "react";

interface ServiceListProps {
    services: Array<IService>
}
 
const ServiceList: FunctionComponent<ServiceListProps> = ({services}) => {
    return (
        <Container className="grid grid-cols-1 lg:grid-cols-3 gap-5 place-items-center">
            {services.map(service => <ServiceItem key={service.name} service={service} />)}
        </Container>
    );
}
 
export default ServiceList;