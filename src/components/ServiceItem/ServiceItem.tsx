import ButtonCallToAction from "@components/ButtonCallToAction/ButtonCallToAction";
import Container from "@components/Container/Container";
import Layout from "@components/Layout/Layout";
import { IService } from "@models/Service";
import { FunctionComponent, createElement } from "react";

interface ServiceItemProps {
    service: IService
}
 
const ServiceItem: FunctionComponent<ServiceItemProps> = ({service}) => {
    const IconElement = service.icon;

    return (
        <Layout className="bg-terra-50 col-span-1 rounded-t-full h-96 rounded-b-xl shadow-xl grid gap-2 max-w-xs hover:scale-110 transition-all">
            <Container className="rounded-t-full rounded-b-xl border-2 border-gray-50">
                <Container className="rounded-t-full h-full rounded-b-xl border-2 border-gray-50 grid place-items-center">
                    <IconElement className="w-12 h-12 text-terra-500"/>
                </Container>
            </Container>
            <Container className="border-2 border-gray-50 h-full">
                <h4 className="font-bold text-xl">{service.name}</h4>
                <p className="text-justify text-sm">{service.description}</p>
                <ButtonCallToAction>Demander un Devis</ButtonCallToAction>
            </Container>
        </Layout>
    );
}
 
export default ServiceItem;