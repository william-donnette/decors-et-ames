import ButtonCallToAction from "@components/ButtonCallToAction/ButtonCallToAction";
import Container from "@components/Container/Container";
import Layout from "@components/Layout/Layout";
import Screen from "@components/Screen/Screen";
import { ArrowSmallDownIcon } from "@heroicons/react/24/solid";
import { asset } from "@utils/functions";
import { FunctionComponent } from "react";

interface HomeHeroProps {}

const HomeHero: FunctionComponent<HomeHeroProps> = () => {
  return (
    <Screen className="bg-terra-50 h-screen flex flex-col">
      <Layout className="flex flex-col md:flex-row relative h-3/4">
        <Container className="z-10">
          <h1>Laura Larson<br/>Décoratrice d'Intérieur</h1>
          <p>Je vous accompagne dans vos projets de décoration et d'aménagement d'intérieur.</p>
          <ButtonCallToAction>Me Contacter</ButtonCallToAction>
        </Container>
        <Container className="z-0 absolute md:static">
          <img
            src={asset("/assets/img/scandinav-living-room.jpg")}
            alt="Montpellier Décoration Intérieur Salon Scandinave"
            className="rounded-xl blur-sm opacity-50 md:blur-none md:opacity-100"
          />
        </Container>
      </Layout>
      <Layout className="grid grid-cols-1 gap-2 md:gap-5 col-span-12 place-items-center h-1/4">
        <h3 className="text-xl">Envie de changer d'univers ?</h3>
        <div className="p-2 bg-terra-500 shadow rounded-full w-min animate-bounce">
          <ArrowSmallDownIcon className="text-terra-100 w-6 h-6" />
        </div>
      </Layout>
    </Screen>
  );
};

export default HomeHero;
