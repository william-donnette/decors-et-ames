import Layout from "@components/Layout/Layout";
import Screen from "@components/Screen/Screen";
import ServiceItem from "@components/ServiceItem/ServiceItem";
import ServiceList from "@components/ServiceList/ServiceList";
import { IService } from "@models/Service";
import ServiceRepository from "@repository/ServiceRepository";
import { FunctionComponent } from "react";

interface ServiceCallToActionProps {
    
}
 
const ServiceCallToAction: FunctionComponent<ServiceCallToActionProps> = () => {

    const services: Array<IService> = ServiceRepository.getServices();

    return (
      <Screen className="bg-terra-500">
        <Layout className="flex flex-col justify-between items-center h-full">
          <h2 className="text-white text-5xl">Découvrez mes prestations</h2>
          <ServiceList services={services}/>
        </Layout>
      </Screen>
    );
}
 
export default ServiceCallToAction;