import Container from "@components/Container/Container";
import CustomerReviewItem from "@components/CustomerReviewItem/CustomerReviewItem";
import Layout from "@components/Layout/Layout";
import { ICustomerReview } from "@models/CustomerReview";
import { FunctionComponent } from "react";

interface CustomerReviewListProps {
    customerReviews: Array<ICustomerReview>
}
 
const CustomerReviewList: FunctionComponent<CustomerReviewListProps> = ({customerReviews}) => {
    return (
        <Container className="grid gap-5" >
            {customerReviews.map((customerReview, index) => <CustomerReviewItem customerReview={customerReview} />)}
        </Container>
    );
}
 
export default CustomerReviewList;