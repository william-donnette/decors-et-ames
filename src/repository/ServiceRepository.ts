import { IService } from "@models/Service";
import FactoryRepository from "./FactoryRepository";
import { ChatBubbleLeftRightIcon, ComputerDesktopIcon, HomeModernIcon } from "@heroicons/react/24/solid";

export default class ServiceRepository {
    static #services: Array<IService>;

    static getServices (): Array<IService> {
        if (!this.#services) {
            this.#services = [
                FactoryRepository.getServiceFactory().create({
                    name: 'Prestation 1',
                    description: 'Une courte description décrivant la prestation',
                    price: 100,
                    icon: ChatBubbleLeftRightIcon
                }),
                FactoryRepository.getServiceFactory().create({
                    name: 'Prestation 2',
                    description: 'Une courte description décrivant la prestation',
                    price: 100,
                    icon: ComputerDesktopIcon
                }),
                FactoryRepository.getServiceFactory().create({
                    name: 'Prestation 3',
                    description: 'Une courte description décrivant la prestation',
                    price: 100,
                    icon: HomeModernIcon
                })
            ]
        }
        return this.#services
    }
}