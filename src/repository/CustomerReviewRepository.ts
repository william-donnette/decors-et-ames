import { ICustomerReview } from "@models/CustomerReview"
import FactoryRepository from "./FactoryRepository";
import { asset } from "@utils/functions";

export default class CustomerReviewRepository {
    static #customerReviews: Array<ICustomerReview>

    static getCustomerReviews () {
        if (!this.#customerReviews) {
            this.#customerReviews = [
                FactoryRepository.getCustomerReviewFactory().create({
                    review: "Laura est incroyable. Elle a su créer un espace salon dans lequel je me sens bien. Tout en respectant mon budget !",
                    customerName: "M. Dupont",
                    customerImage: asset("/assets/img/faces/black-man-shorthair.webp")
                }),
                FactoryRepository.getCustomerReviewFactory().create({
                    review: "Laura est aussi merveilleuse que ma nouvelle cuisine !",
                    customerName: "Mme. Dupont",
                    customerImage: asset("/assets/img/faces/mixed-woman-shorthair.webp")
                }),
                FactoryRepository.getCustomerReviewFactory().create({
                    review: "PARFAIT !!!",
                    customerName: "M. Doe",
                    customerImage: asset("/assets/img/faces/white-farmer-redhair.webp")
                }),
            ]
        }
        return this.#customerReviews;
    }
}