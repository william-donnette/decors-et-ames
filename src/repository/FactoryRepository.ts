import CustomerReviewFactory, { ICustomerReviewFactory } from "@factory/CustomerReviewFactory";
import ServiceFactory, { IServiceFactory } from "@factory/ServiceFactory";

export default class FactoryRepository {
    static #serviceFactory: IServiceFactory;
    static #customerReviewFactory: ICustomerReviewFactory;

    static getServiceFactory (): IServiceFactory {
        if (!this.#serviceFactory) {
            this.#serviceFactory = new ServiceFactory();
        }
        return this.#serviceFactory;
    }

    static getCustomerReviewFactory (): ICustomerReviewFactory {
        if (!this.#customerReviewFactory) {
            this.#customerReviewFactory = new CustomerReviewFactory();
        }
        return this.#customerReviewFactory;
    }
}