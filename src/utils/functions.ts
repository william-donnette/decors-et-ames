import APP from "@config/app";
import TAILWIND from "@config/tailwind";

export const classNames = (...classes: string[]) => {
    return classes.filter(Boolean).join(TAILWIND.CLASSNAME_DELIMITER);
}

export const asset = (path: string) => {
    if (APP.ENV === "local") return APP.PUBLIC_PATH + path;
    return path;
}