const APP = {
    ENV: import.meta.env.VITE_APP_ENV ?? "local",
    PUBLIC_PATH: import.meta.env.VITE_APP_PUBLIC_PATH ?? "/public",
}

export default APP;