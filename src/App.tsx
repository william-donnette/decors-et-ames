import { asset } from "@utils/functions";
import { ArrowSmallDownIcon } from '@heroicons/react/24/solid'
import HomePage from "@pages/HomePage/HomePage";
import Footer from "@components/Footer/Footer";

function App() {

  return (
    <>
      <HomePage />
      <Footer />
    </>
  );
}

export default App;