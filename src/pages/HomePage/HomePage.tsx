import CustomerReview from "@components/CustomerReview/CustomerReview";
import HomeHero from "@components/HomeHero/HomeHero";
import ProjectCallToAction from "@components/ProjectCallToAction/ProjectCallToAction";
import Separator from "@components/Separator/Separator";
import ServiceCallToAction from "@components/ServiceCallToAction/ServiceCallToAction";
import { FunctionComponent } from "react";

interface HomePageProps {
    
}
 
const HomePage: FunctionComponent<HomePageProps> = () => {
    return (
        <>
            <HomeHero />
            <ServiceCallToAction />
            <ProjectCallToAction />
            <Separator />
            <CustomerReview />
        </>
    );
}
 
export default HomePage;