import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react'
import path from "path";


export default ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  const plugins = [react()];
  return defineConfig({
    plugins,
    publicDir: 'src/assets',
    resolve: {
      alias: {
        '@panels' : path.resolve(__dirname, './src/panels'),
      '@api' : path.resolve(__dirname, './src/api'),
      '@components' : path.resolve(__dirname, './src/components'),
      '@config' : path.resolve(__dirname, './src/config'),
      '@factory' : path.resolve(__dirname, './src/factory'),
      '@hooks' : path.resolve(__dirname, './src/hooks'),
      '@languages' : path.resolve(__dirname, './src/languages'),
      '@maillons' : path.resolve(__dirname, './src/maillons'),
      '@middlewares' : path.resolve(__dirname, './src/middlewares'),
      '@models' : path.resolve(__dirname, './src/models'),
      '@pages' : path.resolve(__dirname, './src/pages'),
      '@providers' : path.resolve(__dirname, './src/providers'),
      '@reducers' : path.resolve(__dirname, './src/reducers'),
      '@repository' : path.resolve(__dirname, './src/repository'),
      '@routes' : path.resolve(__dirname, './src/routes'),
      '@utils' : path.resolve(__dirname, './src/utils')
      }
    },
    build: {
      chunkSizeWarningLimit: 1500,
    },
  });
};
